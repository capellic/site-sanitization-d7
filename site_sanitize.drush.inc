<?php

define('DRUPAL_VERSION', drush_drupal_major_version());
// Site specific sanitization goes in the site_sanitize.site.inc file, not in
// the Drush command file (site_sanitize.drush.inc).
include_once 'site_sanitize.site.inc';

require __DIR__ . '/src/SiteSanitizer.php';

// Drupal 8 uses the core Random class.
if (DRUPAL_VERSION == 7) {
  require __DIR__ . '/src/Random.php';
}

/**
 * Implements hook_drush_command().
 */
function site_sanitize_drush_command() {

  $commands['site-sanitize'] = array(
    'description' => 'Site user and custom DB sanitization.',
    'aliases' => array('sitesan'),
    'arguments' => array(),
    'options' => array(
      'status' => 'Do not sanitize. Only report the status.',
      'preserve-tables' => 'Do not truncate tables.',
      'preserve-users' => 'Do not sanitize users.',
    ),
    'examples' => array(
      'drush sitesan' => 'Sanitize the site',
      'drush sitesan --status' => 'Do not sanitize the site. Only report the status.',
      'drush sitesan --preserve-tables' => 'Sanitize the site but do not truncate ANY tables',
      'drush sitesan --preserve-users' => 'Sanitize the site but do not sanitize ANY users',
    ),
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUPAL_MAX',
  );

  return $commands;
}

/**
 * Drush command logic.
 */
function drush_site_sanitize() {

  // Not on live.
  if (defined('PANTHEON_ENVIRONMENT') && (PANTHEON_ENVIRONMENT === 'live')) {
    drush_log('The live environment may not be sanitized!', 'error');
    return;
  }

  if (DRUPAL_VERSION != '7' && DRUPAL_VERSION != '8') {
    drush_log('Only Drupal 7 and 8 are supported! This site is on Drupal ' . DRUPAL_VERSION . '.', 'error');
    return;
  }

  $sanitizer = new SiteSanitizer('@self');

  $status_only = drush_get_option('status', FALSE);
  $preserve_tables = drush_get_option('preserve-tables', FALSE);
  $preserve_users = drush_get_option('preserve-users', FALSE);

  $sanitizer = new SiteSanitizer('@self');

  if ($status_only) {
    $sanitizer->statusGet('site_sanitization_status');
    return;
  }

  site_sanitization_print('***** Site sanitize underway *****');

  $sanitizer->start();

  // Call the pre sanitize hook from the inc file.
  if (function_exists('site_sanitize_pre')) {
    site_sanitization_print('----- Running pre-sanitize hook -----');
    site_sanitize_pre();
  }

  $sanitizer->clearCache();
  if (!$preserve_users) {
    $sanitizer->sanitizeUsers();
  }
  else {
    site_sanitization_print('NOT SANITIZING ANY USERS!');
  }
  if (!$preserve_tables) {
    $sanitizer->truncateTables(site_sanitize_tables());
  }
  else {
    site_sanitization_print('NOT TRUNCATING ANY TABLES!');
  }
  $sanitizer->sanitizeFields(site_sanitize_fields());

  // Call the post sanitize hook from the inc file.
  if (function_exists('site_sanitize_post')) {
    site_sanitization_print('----- Running post-sanitize hook -----');
    site_sanitize_post();
  }

  $sanitizer->complete();
}

/** 
 * Print progress messaegs to CLI and Watchdog.
 * 
 * @param $message string
 *    The message.
 * @param $mode string
 *    (optional) Possible values are "cli" and "watchog". Allows us to write to 
 *    one output.
 * @param $severity string 
 *    (optional) Watchdog severity.
 */
function site_sanitization_print($message, $mode = '', $severity = 'WATCHDOG_NOTICE') {
  // CLI
  if (empty($mode) || ($mode == 'cli')) {
    if (is_array($message)) {
      drush_print_r($message);
    }
    else {
      drush_print(dt("\n" . ' ' . $message));
    }
  }

  // Watchdog call wrapped in drush_invoke_process due to watchdog() not being
  // called when invoked directly.
  if (empty($mode) || ($mode == 'watchdog')) {
    if (is_array($message)) {
      $message = '<pre>' . print_r($message, TRUE) . '</pre>';
    }
    else {
      $message = nl2br($message);
    }
    switch (DRUPAL_VERSION) {
      case '7' :
        $watchdog_command = "watchdog('site_sanitization', '" . $message . "', array(), $severity);";
        break;

      case '8' :
        // TBD This is hard to do.
        break;
    }

    drush_invoke_process('@self', 'eval', [
      $watchdog_command,
      'quiet' => 1,
    ]);
  }
}
