<?php

/**
 * Site specific customizations to the sanitization command go here.
 */

/**
 * Provides list of role machine names whose email addresses are not sanitized.
 *
 * @return array
 *   Role names.
 */
function site_sanitize_email_roles() {
  return [
    'administrator',
  ];
}

/**
 * Provides list of tables to be truncated.
 *
 * @return array
 *   Table names to be truncated. Include data and revision tables in needed.
 */
function site_sanitize_tables() {
  return [
  ];
}

/**
 * Provides list of fields to be sanitized. For when you don't want to truncate
 * the entire table.
 *
 * Array item examples [entity].[field_name]:
 *
 * node.body
 * node.field_email_address
 * block_content.body
 * paragraph.field_personal_info
 * taxonomy.field_secret_code
 * user.field_my_private_stuff
 *
 * @return array
 *   Field names to be truncated using the format entity.field_name
 */
function site_sanitize_fields() {
  // Add the field machine names. The main command will update data and revision
  // tables.
  return [
  ];
}

// These pseudo hooks are called from the main drush command.
/**
 * Do stuff before the users, tables and fields are dealt with.
 */
function site_sanitize_pre() {
}

/**
 * Do stuff after the users, tables and fields are dealt with.
 */
function site_sanitize_post() {
}
