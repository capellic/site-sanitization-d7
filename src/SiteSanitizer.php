<?php

/**
 * Site Sanitizer class.
 *
 * Very loosely based on https://github.com/drush-ops/drush/commit/2716c0086d4063eecf746d24620f239c7f9fb874
 *
 */
class SiteSanitizer {

  /**
   * @var string
   *   The site. I.e., the value of -l or --uri flags for drush.
   */
  protected $site;

  /**
   * @var string
   *   The Drupal version.
   */
  public $drupal_version;

  /**
   * Sanitizer constructor.
   *
   * @param string $site
   *   The site. I.e., the value of -l or --uri flags for drush.
   */
  public function __construct($site) {
    $this->site = $site;
    $this->drupal_version = drush_drupal_major_version();
  }

  /**
   * Clear the Drupal cache.
   **/
  public function clearCache() {

    switch ($this->drupal_version) {
      case '7' :
        site_sanitization_print('----- Clearing cache -----');
        drush_invoke_process('@self', 'cache-clear', [
          'all',
          'quiet' => 1,
        ]);
        break;

      case '8' :
        site_sanitization_print('----- Rebuilding cache -----');
        drush_invoke_process('@self', 'cache-rebuild', [
          'all',
          'quiet' => 1,
        ]);
        break;
    }
  }

  /**
   * Sanitize string fields associated with the user.
   **/
  public function sanitizeUsers() {

    $randomizer = $this->getRandomizer();
    $roles = site_sanitize_email_roles();

    site_sanitization_print('----- Sanitizing user email addresses and passwords -----');

    if (!empty($roles)) {
      site_sanitization_print('Preserving email addresses for the ' . implode(', ', $roles) . ' roles');
    }

    $roles = "'" . implode("','", $roles) . "'";

    // Random string from here https://stackoverflow.com/a/39421220
    $mail = "concat(lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0),'@example.com')";
    $pass = $randomizer->name(40);

    // Preserve certain roles' emails.
    switch ($this->drupal_version) {
      case '7':
        $sql = "UPDATE users SET users.mail=$mail, users.init=$mail  WHERE users.uid NOT IN (SELECT u3.uid FROM (SELECT u.uid FROM users u LEFT JOIN users_roles ur on u.uid=ur.uid JOIN role r on ur.rid = r.rid AND r.name IN($roles)) as u3) AND users.uid !=0;";
        break;
      case '8':
        $sql = "UPDATE users_field_data d SET d.mail=$mail, d.init=$mail WHERE d.uid NOT IN (SELECT u3.uid FROM (SELECT u.uid FROM users_field_data u JOIN user__roles ur on u.uid=ur.entity_id AND ur.roles_target_id IN($roles)) as u3) AND d.uid !=0;";
        break;
    }
    $return = drush_invoke_process('@self', 'sql-query', [$sql]);

    if ($return['error_status']) {
      $message = 'Drush reports an error. Sanitization may have failed!';
      drush_log($message, 'error');
      site_sanitization_print($message, 'watchdog');
      $errors[] = $return['error_log'];
      site_sanitization_print($errors);
      site_sanitization_print($sql);
    }
    switch ($this->drupal_version) {
      case '7':
        $sql = "UPDATE users SET pass='$pass' WHERE uid !=0;";
        break;
      case '8':
        $sql = "UPDATE users_field_data SET pass='$pass' WHERE uid !=0;";
        break;
    }
    $return = drush_invoke_process('@self', 'sql-query', [$sql]);

    if ($return['error_status']) {
      $message = 'Drush reports an error. Sanitization may have failed!';
      drush_log($message, 'error');
      site_sanitization_print($message, 'watchdog');
      $errors[] = $return['error_log'];
      site_sanitization_print($errors);
      site_sanitization_print($sql);
      $this->statusSet('site_sanitization_status', 'error_sanitizeUsers_' . date_format(date_create(),"Y/m/d H:i:s e"));
    }
  }

  /**
   * Truncate tables.
   *
   * @param $tables array
   *   Array of table names that will be truncated.
   */
  public function truncateTables($tables) {
    site_sanitization_print('----- Truncating site specific tables -----');

    $errors = [];
    $webform_tables = [];
    $default_tables = ['batch', 'queue'];

    $return = drush_invoke_process('@self', 'pm-info', ['webform'], ['quiet' => 1]);
    if (isset($return['object']['webform']['status']) && $return['object']['webform']['status'] == 'enabled') {
      switch ($this->drupal_version) {
        case '7':
          $webform_tables = [
            'webform_submitted_data',
            'webform_submissions'
          ];
          break;
        case '8':
          $webform_tables = [
            'webform_submission',
            'webform_submission_data',
            'webform_submission_log'
          ];
          break;
      }
    }

    $tables = array_merge($default_tables, $webform_tables, $tables);

    if (!empty($tables)) {
      $sql = '';
      foreach ($tables as $table) {
        $sql = ' TRUNCATE ' . $table . ';';
        $return = drush_invoke_process('@self', 'sql-query', [$sql]);
        if ($return['error_status']) {
          site_sanitization_print(dt('!table', ['!table' => $table]));
          $message = 'Table ' . $table . ' sanitization failed!';
          drush_log(' ' . $message , 'error');
          site_sanitization_print($message, 'watchdog');
          $errors[] = $return['error_log'];
        }
        else {
          site_sanitization_print(dt('!table truncated.', ['!table' => $table]));
        }
      }
      if (!empty($errors)) {
        $message = 'Drush reports an error. Sanitization may have failed!';
        drush_log($message, 'error');
        site_sanitization_print($message, 'watchdog');
        site_sanitization_print($errors);
        $this->statusSet('site_sanitization_status', 'error_truncateTables_' . date_format(date_create(),"Y/m/d H:i:s e"));

      }
    }
  }

  /**
   * Obscure specific field values.
   *
   * @param $fields array
   *   Array of field machine names that will have their data and revision tables
   *   sanitized
   */
  public function sanitizeFields($fields) {
    site_sanitization_print('----- Sanitizing site specific field values -----');

    if (empty($fields)) {
      site_sanitization_print('No fields to be sanitized.');
      return;
    }

    $errors = [];

    // Get the field data so we can see the type.
    switch ($this->drupal_version) {
      case '7':
        $return = drush_invoke_process('@self', 'field-info', ['fields'], [
          'format' => 'php',
          'quiet' => 1,
        ], FALSE);
        $info = $return['output'];
        $info = unserialize($info);
        if ($return['error_status']) {
          $message = 'Drush reports an error. Sanitization may have failed!';
          drush_log($message, 'error');
          site_sanitization_print($message, 'watchdog');
          $errors[] = $return['error_log'];
        }

        foreach ($fields as $field) {
          $data = $info[$field];
          $type = $data['type'];
          $randomizer = $this->getRandomizer();

          switch ($type) {

            case 'email':
              $this->sanitizeTableColumn($field, $randomizer->name(10) . '@example.com', 'email');
              break;

            case 'string_long':
              $this->sanitizeTableColumn($field, $randomizer->sentences(1));
              break;

            case 'telephone':
              $this->sanitizeTableColumn($field, '15555555555');
              break;
            // Using string case for this for now. maybe this works better in Drupal 8,
            // but it's too long for D7.
//          case 'text':
//            $this->sanitizeTableColumn($field, $randomizer->paragraphs(2));
//            break;

            case 'text_long':
              $this->sanitizeTableColumn($field, $randomizer->paragraphs(10));
              break;

            case 'text_with_summary':
              $this->sanitizeTableColumn($field, $randomizer->paragraphs(2));
              $this->sanitizeTableColumn($field . '_summary', $randomizer->name(60));
              break;

            case 'location':
              $this->sanitizeTableColumn($field, 0, 'lid');
              break;

            case 'string':
            case 'text':
            default:
              $this->sanitizeTableColumn($field, $randomizer->name(10));
              break;
          }
        }
        break;

      case '8':
        foreach ($fields as $field) {
          $return = drush_invoke_process('@self', 'config-get', [
            'field.storage.' . $field,
            'type',
          ], [
            'format' => 'php',
          ], FALSE);

          $info = $return['object'];

          if ($return['error_status']) {
            $message = 'Drush reports an error. Sanitization may have failed!';
            drush_log($message, 'error');
            site_sanitization_print($message, 'watchdog');
            $errors[] = $return['error_log'];
          }

          $type = array_pop($info);

          $randomizer = $this->getRandomizer();

          switch ($type) {

            case 'email':
              $this->sanitizeTableColumn($field, $randomizer->name(10) . '@example.com');
              break;

            case 'string_long':
              $this->sanitizeTableColumn($field, $randomizer->sentences(1));
              break;

            case 'telephone':
              $this->sanitizeTableColumn($field, '15555555555');
              break;

            case 'text':
              $this->sanitizeTableColumn($field, $randomizer->paragraphs(2));
              break;

            case 'text_long':
              $this->sanitizeTableColumn($field, $randomizer->paragraphs(10));
              break;

            case 'text_with_summary':
              $this->sanitizeTableColumn($field, $randomizer->paragraphs(2));
              $this->sanitizeTableColumn($field . '_summary', $randomizer->name(255));
              break;

            case 'string':
            default:
              $this->sanitizeTableColumn($field, $randomizer->name(255));
              break;
          }
          if ($return['error_status']) {
            $message = $type . ' field ' . $field . ' sanitization failed!';
            drush_log($message, 'error');
            site_sanitization_print($message, 'watchdog');
          }
          else {
            site_sanitization_print($type . ' field ' . $field . ' sanitized');
          }
        }
    }

    if (!empty($errors)) {
      $message = 'Drush reports an error. Sanitization may have failed!';
      drush_log($message, 'error');
      site_sanitization_print($message, 'watchdog');
      site_sanitization_print($errors);
      $this->statusSet('site_sanitization_status', 'error_sanitizeFields_' . date_format(date_create(),"Y/m/d H:i:s e"));

    }
  }

  /**
   * Replaces all values for a given field with the provided replacement value.
   *
   * @param string $field
   *   The field name name.
   * @param string $sanitized
   *   The replacement value for the field.
   * @param $suffix
   *   Most value columns use 'value', but other need something like 'email' or
   *   'link'.
   */
  public function sanitizeTableColumn($field, $sanitized, $suffix = 'value') {
    $errors = [];

    switch ($this->drupal_version) {
      case '7':
        $data = 'field_data_' . $field;
        $rev = 'field_revision_' . $field;
        $value = $field . '_' . $suffix;
        $sql = "UPDATE $data SET $value='$sanitized'; UPDATE $rev SET $value='$sanitized'; ";
        $return = drush_invoke_process('@self', 'sql-query', [$sql]);
        if ($return['error_status']) {
          $message = 'Field ' . $field . ' sanitization failed!';
          drush_log(' ' . $message, 'error');
          site_sanitization_print($message, 'watchdog');
          $errors[] = $return['error_log'];
          site_sanitization_print($sql);
        }
        else {
          site_sanitization_print(' Field ' . $field . ' sanitized');
        }
        break;

      case '8':
        list ($entity, $field) = explode('.', $field);
        $data = $entity . '__' . $field;
        $value = $field . '_' . $suffix;
        $sql = "UPDATE $data SET $value='$sanitized';";
        if ($entity == 'node') {
          $rev = $entity . '_revision__' . $field;
          $sql.= " UPDATE $rev SET $value='$sanitized'; ";
        }
        $return = drush_invoke_process('@self', 'sql-query', [$sql]);

        if ($return['error_status']) {
          $message = 'Field ' . $field . ' sanitization failed!';
          drush_log(' ' . $message, 'error');
          site_sanitization_print($message, 'watchdog');
          $errors[] = $return['error_log'];
          site_sanitization_print($sql);
        }
        break;
    }
    if (!empty($errors)) {
      $message = 'Drush reports an error. Sanitization may have failed!';
      drush_log($message, 'error');
      site_sanitization_print($message, 'watchdog');
      site_sanitization_print($errors);
      $this->statusSet('site_sanitization_status', 'error_sanitizeTableColumn_' . date_format(date_create(),"Y/m/d H:i:s e"));
    }

  }

  public function getRandomizer() {
    switch ($this->drupal_version) {
      case '7':
        return new Random;
      case '8':
        return new \Drupal\Component\Utility\Random();
    }

  }

  public function start() {
    $this->statusSet('site_sanitization_status', 'running_' . date_format(date_create(),"Y/m/d H:i:s e"));
  }

  public function complete() {
    $site_sanitization_status = $this->statusGet('site_sanitization_status');
    if (stripos($site_sanitization_status, 'error') === FALSE) {
      $site_sanitization_status = $this->statusSet('site_sanitization_status', 'complete_' . date_format(date_create(),"Y/m/d H:i:s e"));
      drush_log(dt($site_sanitization_status), 'success');
      $message = '***** Site sanitizing completed successfully! *****';
      drush_log(dt($message), 'success');
      site_sanitization_print($message, 'watchdog');
    }
    else {
      drush_log(dt($site_sanitization_status), 'error');
      site_sanitization_print($site_sanitization_status, 'watchdog');
      $message = '***** Site sanitizing completed with errors! *****';
      drush_log(dt($message), 'error');
      site_sanitization_print($message, 'watchdog');
    }
  }

  public function statusGet($key) {

    switch (DRUPAL_VERSION) {
      case '7':
        return drush_invoke_process('@self', 'vget', [$key, 'quiet' => 1])['output'];
        break;
      case '8':
        return drush_invoke_process('@self', 'sget', [$key, 'quiet' => 1])['output'];
        break;
    }
  }


  public function statusSet($key, $value) {

    switch (DRUPAL_VERSION) {
      case '7':
        return drush_invoke_process('@self', 'vset', [$key, $value, 'quiet' => 1])['output'];
        break;
      case '8':
        return drush_invoke_process('@self', 'sset', [$key, $value, 'quiet' => 1])['output'];
        break;
    }
  }

}
