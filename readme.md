#Site Sanitize Drush Command
**Only Drush 8 is supported**

##Installation
**Drupal 7**  
  The site_sanitize directory goes in `sites/all/drush`.

**Drupal 8**  
The site_sanitize directory goes in `/drush`.

##Use

For help, from the command line run:

#### Lando
`lando drush -h site-sanitize`

#### Non-lando
`drush -h site-sanitize`

###This command does the following:

####Flags:
 * `--status` Do not sanitize. Only report the status.
 * `--preserve-tables` Do not truncate tables.
 * `--preserve-users` Do not sanitize users.

####Default functionality:
 * Sanitizes user password and email.
 * Clears all webform submissions if webform is enabled.

#### Site specific functionality:
site_sanitize.site.example.inc **must** be copied to site_sanitize.site.inc. Edit it
to specify additional site-specific tables to truncate and fields to sanitize.
You can also run custom code pre and post sanitization.

##Integrating with Lando
Add the following to .lando.yml to automatically sanitize the database when pulling or importing:

```
events:
  post-pull:
    - appserver: cd $LANDO_WEBROOT && drush cc drush && drush sitesan

  post-db-import:
    - appserver: cd $LANDO_WEBROOT && drush cc drush && drush sitesan
```
Add the following to .lando.yml to create a handy sanitize command. This saves time when developing site-specific sanitization.
```
tooling:
  sitesan:
    service: appserver
    cmd:
      - "drush"
      - "site-sanitize"
```
Then you can run `lando sitesan` to fire the drush command without importing or pulling.
You could also run `lando drush sitesan`.


##Integrating with Quicksilver

Add a file called site_sanitize.php to the site's quicksilver scripts location.

`/private/scripts/site_sanitize.php`

Add the following into that file:
```
<?php
if (defined('PANTHEON_ENVIRONMENT') && (PANTHEON_ENVIRONMENT !== 'live')) {
  passthru('drush cc drush');
  passthru('drush site-sanitize');
}
```

Add the following to pantheon.yml to sanitize the db when creating new multidevs or copying the database to a non-live environment:
```
# Pantheon workflow scripts:
workflows:
  create_cloud_development_environment:
    after:
      - type: webphp
        description: Sanitize database
        script: private/scripts/site_sanitize.php
  clone_database:
    after:
      - type: webphp
        description: Sanitize database
        script: private/scripts/site_sanitize.php
```
